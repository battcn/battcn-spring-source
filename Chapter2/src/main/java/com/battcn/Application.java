package com.battcn;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Spring源码分析第三章 - 自定义标签及解析
 *
 * @author Levin
 * @since 2018/01/12
 */
public class Application {


    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        String name = (String) context.getBean("battcn");
        System.out.println(name);

    }

}