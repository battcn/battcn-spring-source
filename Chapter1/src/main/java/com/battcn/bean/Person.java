package com.battcn.bean;


/**
 * @author Levin
 * @create 2017/12/20 0020
 */
public class Person {

    private int id;
    private String name;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public Person() {
        System.out.println("构造函数被调用");
    }

    public Person(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public void init() {
        System.out.println("init bean..");
    }
}
