package com.battcn;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.core.io.ClassPathResource;

/**
 * Spring源码分析第一章
 *
 * @author Levin
 * @since 2018/01/09
 */
public class Application {

    public static void main(String[] args) {

        BeanDefinitionRegistry beanFactory = new DefaultListableBeanFactory();
        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory);
        ClassPathResource resource = new ClassPathResource("bean.xml");
        //整个资源加载的切入点。
        reader.loadBeanDefinitions(resource);

    }
}